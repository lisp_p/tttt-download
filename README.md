# 时间不说谎

客户端下载链接： https://gitee.com/lisp_p/tttt-download/releases/tag/v1.0

试用网页版：https://time-tell-the-truth.com/

## Mac系统提示“文件已损坏”

Mac电脑解压后可能会提示“文件已损坏”等常规问题，根本原因是Mac的安全机制，经常使用Mac系统应该都知道

解压之后，执行下面这条命令

```shell
sudo xattr -r -d com.apple.quarantine tttt.app
```

具体可以参考 https://zhuanlan.zhihu.com/p/135948430

## Linux系统启动命令

```shell
# 直接从命令行启动，后台执行，并且将输出重定向到 /dev/null
./tttt > /dev/null &
```